# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
1º Semestre de 2.022

## Turmas
* 03 - Fabricio Ataides Braz
* 12 - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de **aprendizado baseado em projeto**.

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da instrução, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a investigação, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da instrução, para a tutoria, no que diz respeito ao ensino. A perspectiva do aluno muda de passiva para ativa, no que diz respeito ao aprendizado.

Mesmo que nosso foco seja o projeto, observamos a necessidade de estimular a turma a adquirir habilidades fundamentais para o desenvolvimento de algoritmos. Em razão disso, decidimos lançar mão da instrução no início do período letivo. Momento em que passaremos pela sensibilização sobre lógica de programação, usando para isso os recursos de [programação em blocos](https://code.org), e depois fundamentos de [programação em Python](https://github.com/PenseAllen/PensePython2e/).

Após o momento de instrução mencionado anteriormente, serão constituídos grupos em que o aluno, apoioado por até 20 (vinte) tutores (professores e monitores), percorrerá uma trilha de aprendizagem voltada para a construção de dashboards.

## Ferramentas & Materiais
* [Aprender3](https://aprender3.unb.br/course/view.php?id=14603) - Comunicação e trabalho colaborativo
* [Python](https://www.python.org/) - Linguagem de programação
* [Plotly](https://plotly.com/) - Biblioteca de construção de dashboard
* [Gitlab](https://gitlab.com/ensino_unb/apc/2021_1/doc) - Reposotório de código e coloboração
* [Code](https://studio.code.org/home#:~:text=CC%20Intro%20%2D%20APC_AA_BB_2021_2) - Programação em blocos
    * Chave no moodle

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* ExCODE: nota correspondente ao percentual de exercícios resolvidos na plataforma code.org

* ExMoodle: nota correspondente ao percentual de exercícios resolvidos no próprio Moodle

* AGP: avaliação do grupo pelos professores. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/svg.image?%5Cfrac%7BExCode*ExMoodle_1*ExMoodle_2*ExMoodle_3%7D%7B100%5E%7B4%7D%7D*(0.45*%20%5Csqrt%7B%7BAGP_1*AIG_1%7D%7D&plus;%200.55*%20%5Csqrt%7B%7BAGP_2*AIG_2%7D%7D))


Serão dois encontros avaliativos, observando as fases de desenvolvimento, com os seguintes pesos:
1. Desenvolvimento (peso 0,45)
2. Desenvolvimento (peso 0,55)

### Cronograma
| **Aula** | **Data** | **Programação** | **Dia Semana** |
|---|---|---|---|
| 1 | 06/06/2022 | Acolhida | segunda-feira |
| 2 | 07/06/2022 | Code.org | terça-feira |
| 3 | 09/06/2022 | Code.org | quinta-feira |
| 4 | 13/06/2022 | Code.org | segunda-feira |
| 5 | 14/06/2022 | Code.org | terça-feira |
| - | 16/06/2022 | Feriado: Corpus Christi | - |
| 6 | 20/06/2022 | [Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md) | segunda-feira |
| 7 | 21/06/2022 | [Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md) | terça-feira |
| 8 | 23/06/2022 | Prática de Estruturas Sequenciais  | quinta-feira |
| 9 | 27/06/2022 | [Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md). Data limite para envio da lista code.org <br/>  | segunda-feira |
| 10 | 28/06/2022 | [Instalação do Python e interface](https://github.com/PenseAllen/PensePython2e/blob/master/docs/04-caso-interface.md) | terça-feira |
| 11 | 30/06/2022 | Prática de Estruturas Sequenciais  | quinta-feira |
| 12 | 04/07/2022 | Data limite de envio da lista de  estruturas sequenciais <br/> [Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md) | segunda-feira |
| 13 | 05/07/2022 | Prática de Estruturas de Decisão | terça-feira |
| 14 | 07/07/2022 | [Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md) | quinta-feira |
| 15 | 11/07/2022 | Data limite de envio da lista estruturas de decisão <br/>. [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md) | segunda-feira |
| 16 | 12/07/2022 | [Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md) | terça-feira |
| 17 | 14/07/2022 | Prática de Estruturas de Repetição | quinta-feira |
| 18 | 18/07/2022 | [Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md) | segunda-feira |
| 19 | 19/07/2022 | Plotly <br/> Brainstorm & Research <br/> Data limite de envio da lista URI estrutura de repetição | terça-feira |
| 20 | 21/07/2022 | Plotly <br/> Brainstorm & Research | quinta-feira |
| 21 | 25/07/2022 | Plotly <br/> Design | segunda-feira |
| 22 | 26/07/2022 | Plotly <br/> Design | terça-feira |
| 23 | 28/07/2022 | Plotly <br/> Design | quinta-feira |
| 24 | 01/08/2022 | Plotly <br/> Desenvolvimento | segunda-feira |
| 25 | 02/08/2022 | Plotly <br/> Desenvolvimento | terça-feira |
| 26 | 04/08/2022 | Plotly <br/> Desenvolvimento | quinta-feira |
| 27 | 08/08/2022 | Plotly <br/> Desenvolvimento | segunda-feira |
| 28 | 09/08/2022 | Plotly <br/> Desenvolvimento | terça-feira |
| 29 | 11/08/2022 | Plotly <br/> Desenvolvimento | quinta-feira |
| 30 | 15/08/2022 | Plotly <br/> Desenvolvimento | segunda-feira |
| 31 | 16/08/2022 | Plotly <br/> Desenvolvimento | terça-feira |
| 32 | 18/08/2022 | Dash <br/> Desenvolvimento | quinta-feira |
| 33 | 22/08/2022 | Dash <br/> Desenvolvimento | segunda-feira |
| 34 | 23/08/2022 | Avaliação 1 | terça-feira |
| 35 | 25/08/2022 | Avaliação 1 | quinta-feira |
| - | 29/08/2022 | Semana Universitária | - |
| - | 30/08/2022 | Semana Universitária | - |
| - | 01/09/2022 | Semana Universitária | - |
| 36 | 05/09/2022 | Avaliação 1 | segunda-feira |
| 37 | 06/09/2022 | Dash <br/> Desenvolvimento | terça-feira |
| 38 | 08/09/2022 | Dash <br/> Desenvolvimento | quinta-feira |
| 39 | 12/09/2022 | Dash <br/> Desenvolvimento | segunda-feira |
| 40 | 13/09/2022 | Dash <br/> Desenvolvimento | terça-feira |
| 41 | 15/09/2022 | Dash <br/> Desenvolvimento | quinta-feira |
| 42 | 19/09/2022 | Dash <br/> Desenvolvimento | segunda-feira |
| 43 | 20/09/2022 | Avaliação 2 | terça-feira |
| 44 | 22/09/2022 | Avaliação 2 | quinta-feira |


### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade de até 10 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de membros do seu grupo, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para apresentação.

### Evasão
Grupos com menos de 5 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers

